//
//  MainVC.h
//  DownloadingImageAsync
//
//  Created by Александр Малышев on 31.01.15.
//  Copyright (c) 2015 SecurityQQ. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "downloadingImageView.h"

@interface MainVC : UIViewController

@property (weak, nonatomic) IBOutlet downloadingImageView *imageView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;


@end
