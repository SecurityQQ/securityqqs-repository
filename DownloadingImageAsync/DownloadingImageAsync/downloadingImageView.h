//
//  downloadingImageView.h
//  DownloadingImageAsync
//
//  Created by Александр Малышев on 31.01.15.
//  Copyright (c) 2015 SecurityQQ. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface downloadingImageView : UIImageView <NSURLConnectionDataDelegate>
@property (weak, nonatomic) UIActivityIndicatorView *activityIndicator;

- (void) getImageContentsWithURL: (NSURL *) sourceURL usingActivityIndicator: (BOOL) isUsingActivityIndicator;
@end
