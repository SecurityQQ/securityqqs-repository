//
//  downloadingImageView.m
//  DownloadingImageAsync
//
//  Created by Александр Малышев on 31.01.15.
//  Copyright (c) 2015 SecurityQQ. All rights reserved.
//

#import "downloadingImageView.h"

@interface downloadingImageView ()

@property (retain, nonatomic) NSMutableData *imageData;

@end

@implementation downloadingImageView

- (void) getImageContentsWithURL: (NSURL *) sourceURL usingActivityIndicator: (BOOL) isUsingActivityIndicator {
    if( !_imageData ) {
        _imageData = [[NSMutableData alloc] init];
    }
    NSURLRequest *request = [NSURLRequest requestWithURL:sourceURL
                                             cachePolicy:NSURLRequestUseProtocolCachePolicy
                                         timeoutInterval:60.0];
    if( isUsingActivityIndicator ) {
        (assert(_activityIndicator));
        [[self activityIndicator] startAnimating];
    }
    
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
    [NSURLConnection connectionWithRequest:request delegate:self];
}

- (void) connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
    [_imageData appendData:data];
}

- (void) connectionDidFinishLoading:(NSURLConnection *)connection {
    [self setImage:[UIImage imageWithData:_imageData]];
    [_activityIndicator stopAnimating];
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
}



@end
