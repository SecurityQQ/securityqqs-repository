//
//  MainVC.m
//  DownloadingImageAsync
//
//  Created by Александр Малышев on 31.01.15.
//  Copyright (c) 2015 SecurityQQ. All rights reserved.
//

#import "MainVC.h"

NSString* const kPath = @"http://93.175.16.58/web/objc/monkey_selfie.jpg";
@implementation MainVC

- (void)viewDidLoad {
    [super viewDidLoad];
}
- (void) viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)downloadImage:(id)sender {
    [_imageView setImage:nil];
    [_imageView setActivityIndicator: _activityIndicator];
    [_imageView getImageContentsWithURL:[NSURL URLWithString:kPath] usingActivityIndicator:YES];
}
@end
