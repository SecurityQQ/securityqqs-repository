//
//  Person.m
//  MIPT_Obj-C
//
//  Created by Александр Малышев on 27.09.14.
//  Copyright (c) 2014 SecurityQQ. All rights reserved.
//

#import "Person.h"

@implementation Person

//! ------------------------------------------------------------------
//! ------------------------------------------------------------------
//! INITIALISATION OF CONSTS ========================================
//! ------------------------------------------------------------------
//! ------------------------------------------------------------------

#pragma mark initialisation of consts

NSString * const FirstNameKey=@"FirstName: ";
NSString * const LastNameKey=@"LastName: ";

//! ------------------------------------------------------------------
//! END OF INITIALISATION OF CONSTS =================================
//! ------------------------------------------------------------------


//! -----------------------------------------------------------------
//! -----------------------------------------------------------------
//! INIT ============================================================
//! ------------------------------------------------------------------
//! ------------------------------------------------------------------

#pragma mark initialisation

-(instancetype)initWithName: (NSString*) firstName LastName: (NSString*) lastName {
    if (!(self = [self init]))  return nil;
    _firstName = firstName;
    _lastName = lastName;
    return self;
}

- (instancetype)initWithFilePath:(NSString *)filePath {
    
    NSDictionary *person = [[NSDictionary alloc] initWithContentsOfFile: filePath];
    if (!(self = [self init]))  return nil;
    
    if([person[FirstNameKey] isEqualToString:@"n/a"]) _firstName = nil;
    else _firstName = person[FirstNameKey];
    if([person[LastNameKey] isEqualToString:@"n/a"]) _lastName = nil;
    else _lastName = person[LastNameKey];
    
return self;

}

//! ------------------------------------------------------------------
//! ------------------------------------------------------------------
//!END OF INIT ======================================================
//! ------------------------------------------------------------------
//! ------------------------------------------------------------------

//! ------------------------------------------------------------------
//! ------------------------------------------------------------------
//! METHODS ==========================================================
//! ------------------------------------------------------------------
//! ------------------------------------------------------------------

#pragma mark methods

- (void) saveToFilePath: (NSString *)filePath {
    NSString *name, *lastname;
    //checking != nil
    if (! self.firstName)
        name = @"n/a";
    else
        name = self.firstName;
    
    if (! self.lastName)
        lastname = @"n/a";
    else
        lastname = self.lastName;
    
    NSLog(@"%@ %@", name, lastname);
    
    [[[NSDictionary alloc] initWithObjects:@[ name, lastname]  forKeys:@[FirstNameKey, LastNameKey]] writeToFile:filePath atomically:YES];
    // TODO: [NSHomeDirectory() stringByAppendingPathComponent: fileName];
}

//!-------------------------------------------------------------------
//! DESCTIPTION ------------------------------------------------------
//!-------------------------------------------------------------------

- (NSString *) description {
    
    return [NSString stringWithFormat:@"First Name : %@ Last Name: %@", self.firstName, self.lastName];
}

//!-------------------------------------------------------------------
//!-------------------------------------------------------------------
//! END OF METHODS ==================================================
//! -----------------------------------------------------------------
//! -----------------------------------------------------------------

//! ENCODING/DECODING ================================================
#pragma mark encoding/decoding

- (instancetype)initWithCoder:(NSCoder *) aDecoder {
    if (!(self = [super init])) return nil;
    _firstName=[aDecoder decodeObjectForKey:FirstNameKey];
    _lastName=[aDecoder decodeObjectForKey:LastNameKey];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder{
    [aCoder encodeObject:self.firstName forKey:FirstNameKey];
    [aCoder encodeObject:self.lastName forKey:LastNameKey];
}

@end

//! -----------------------------------------------------------------
//! -----------------------------------------------------------------