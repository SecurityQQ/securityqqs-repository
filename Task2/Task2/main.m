//
//  main.m
//  Task2
//
//  Created by Александр Малышев on 04.10.14.
//  Copyright (c) 2014 SecurityQQ. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, const char * argv[]) {
    return NSApplicationMain(argc, argv);
}
