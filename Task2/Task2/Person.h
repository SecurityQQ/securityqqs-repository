//
//  Person.h
//  MIPT_Obj-C
//
//  Created by Александр Малышев on 27.09.14.
//  Copyright (c) 2014 SecurityQQ. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Person : NSObject <NSCoding>

@property (nonatomic) NSString *firstName, *lastName;

- (instancetype) initWithFilePath:(NSString *)filePath;
- (instancetype) initWithName: (NSString *) firstName LastName: (NSString *) lastName;

- (void)saveToFilePath:(NSString *)filePath;


@end
