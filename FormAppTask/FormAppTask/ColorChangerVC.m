//
//  ColorChangerVC.m
//  FormAppTask
//
//  Created by Александр Малышев on 09.02.15.
//  Copyright (c) 2015 SecurityQQ. All rights reserved.
//

#import "ColorChangerVC.h"
#import "UserDataVC.h"

NSString * const kBackSegue = @"Back Segue";

@implementation ColorChangerVC

- (void) viewDidLoad {
    [super viewDidLoad];
    srand((unsigned int) time(NULL));
    [self colors];
    _selectedColor = _colors[rand() % [_colors count]];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if( [segue.identifier isEqualToString: kBackSegue] ) {
        UserDataVC *userDataVC = segue.destinationViewController;
        userDataVC.backgroundColor = _selectedColor;
    }
}

- (NSArray *) colors {
    if (!_colors) {
        self.colors = @[[UIColor blueColor],
                        [UIColor lightGrayColor],
                        [UIColor cyanColor],
                        [UIColor yellowColor],
                        [UIColor magentaColor],
                        [UIColor purpleColor],
                        [UIColor brownColor],
                        [UIColor redColor],
                        [UIColor grayColor],
                        [UIColor whiteColor],
                        [UIColor orangeColor]];
    }
    return _colors;
}



@end
