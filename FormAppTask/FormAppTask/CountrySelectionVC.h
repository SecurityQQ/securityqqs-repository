//
//  CountrySelectionVC.h
//  FormAppTask
//
//  Created by Александр Малышев on 05.02.15.
//  Copyright (c) 2015 SecurityQQ. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CountrySelectionVC : UIViewController<UIPickerViewDataSource, UIPickerViewDelegate>

@property (weak, nonatomic) IBOutlet UIPickerView *countryPicker;
@property (strong, nonatomic) NSMutableArray *countries;
@property (weak, nonatomic) NSString *selectedCountry;
@end
