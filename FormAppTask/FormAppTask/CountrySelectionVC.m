//
//  CountrySelectionVC.m
//  FormAppTask
//
//  Created by Александр Малышев on 05.02.15.
//  Copyright (c) 2015 SecurityQQ. All rights reserved.
//

#import "CountrySelectionVC.h"
#import "UserDataVC.h"

NSString * const CountryDidSelected = @"CountryDidSelected";

@implementation CountrySelectionVC

- (void) viewDidLoad {
    [super viewDidLoad];
    [self setDataForCountries];
}

- (void) setDataForCountries {
    
    NSString *localLanguage = [[[NSBundle mainBundle] preferredLocalizations] objectAtIndex:0];
    NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier: localLanguage];
    NSArray *countryCodes = [NSLocale ISOCountryCodes];
    _countries = [[NSMutableArray alloc] initWithCapacity:[countryCodes count]];
    
    for (NSString *countryCode in countryCodes){
        NSString *identifier = [NSLocale localeIdentifierFromComponents: [NSDictionary dictionaryWithObject: countryCode forKey: NSLocaleCountryCode]];
        NSString *country = [locale displayNameForKey: NSLocaleIdentifier value: identifier];
        [_countries addObject: country];
    }
}

- (NSInteger) numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}

- (NSInteger) pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    return [_countries count];
}

- (NSString *) pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    return [_countries objectAtIndex:row];
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    _selectedCountry = _countries[row];
    [self performSegueWithIdentifier: CountryDidSelected sender:self];
    
}

- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
//    <#QUESTION:#>Is it ok?
    if( [[segue identifier] isEqualToString:CountryDidSelected] ) {
        UserDataVC *usv = segue.destinationViewController;
        if( !usv.countryTextView ) {
            usv.countryTextView = [[UITextView alloc] init];
        }
        usv.userCountry = _selectedCountry;
    }
}

@end
