//
//  UserDataVC.h
//  FormAppTask
//
//  Created by Александр Малышев on 05.02.15.
//  Copyright (c) 2015 SecurityQQ. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UserDataVC : UIViewController
@property (strong, nonatomic) IBOutlet UITextField *nameTextField;
@property (strong, nonatomic) IBOutlet UITextView *countryTextView;
@property (weak, nonatomic) NSString *userCountry;
@property (weak, nonatomic) NSString *userName;
@property (weak, nonatomic) NSUserDefaults *userDefaults;
@property (weak, nonatomic) UIColor *backgroundColor;
@end
