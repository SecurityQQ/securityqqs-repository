//
//  main.m
//  FormAppTask
//
//  Created by Александр Малышев on 05.02.15.
//  Copyright (c) 2015 SecurityQQ. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
