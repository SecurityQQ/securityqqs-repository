//
//  ColorChangerVC.h
//  FormAppTask
//
//  Created by Александр Малышев on 09.02.15.
//  Copyright (c) 2015 SecurityQQ. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ColorChangerVC : UIViewController

@property (strong, nonatomic) NSArray *colors;
@property (strong, nonatomic) UIColor *selectedColor;
@property (weak, nonatomic) IBOutlet UIButton *backButton;

@end
