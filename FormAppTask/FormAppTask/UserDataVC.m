//
//  UserDataVC.m
//  FormAppTask
//
//  Created by Александр Малышев on 05.02.15.
//  Copyright (c) 2015 SecurityQQ. All rights reserved.
//

#import "UserDataVC.h"

NSString * const kUserCountry = @"User Country";
NSString * const kUserName = @"User Name";

@implementation UserDataVC

- (void) viewDidLoad {
    [super viewDidLoad];
    [self setUserDefaults: [NSUserDefaults standardUserDefaults]];
    if( _userCountry ) {
        [_userDefaults setObject:_userCountry forKey:kUserCountry];
    }
}

- (IBAction)nameFieldEditingEnded:(id)sender {
    [_userDefaults setObject:[_nameTextField text] forKey:kUserName];
}

- (void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    NSString *countryFromDefaults = [_userDefaults objectForKey:kUserCountry];
    NSString *nameFromDefaults =
    [_userDefaults objectForKey:kUserName];
    [_nameTextField setText:nameFromDefaults];
    _userCountry = [_userDefaults objectForKey:kUserCountry];
    if( countryFromDefaults ) {
        [_countryTextView setText:_userCountry];
    }
}

-(IBAction)returnFromColorChanging:(UIStoryboardSegue *)segue {
    self.view.backgroundColor =  _backgroundColor;
}

@end
