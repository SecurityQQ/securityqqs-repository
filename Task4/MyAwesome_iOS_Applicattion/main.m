//
//  main.m
//  MyAwesome_iOS_Applicattion
//
//  Created by Александр Малышев on 11.10.14.
//  Copyright (c) 2014 SecurityQQ. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
