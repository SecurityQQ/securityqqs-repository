//
//  ViewController.h
//  MyAwesome_iOS_Applicattion
//
//  Created by Александр Малышев on 11.10.14.
//  Copyright (c) 2014 SecurityQQ. All rights reserved.
//

#import <UIKit/UIKit.h>
@import Security; // <=> #import <Security/Security.h> + adding framework
@interface ViewController : UIViewController

@property (weak, nonatomic) IBOutlet UITextField *PasswordTextField;

@property (strong, nonatomic) IBOutlet UIProgressView *PasswordDifficultyProgressBar;
@property (nonatomic, retain) NSTimer *myTimer;

//- (int) PasswordDifficulty: int Password;

@end

