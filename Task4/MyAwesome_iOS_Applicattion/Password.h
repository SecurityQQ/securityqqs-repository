//
//  Password.h
//  MyAwesome_iOS_Applicattion
//
//  Created by Александр Малышев on 25.10.14.
//  Copyright (c) 2014 SecurityQQ. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Password : NSObject

@property NSString * password;
- (float) getPasswordDifficulty;

@end
