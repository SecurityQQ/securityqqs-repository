//
//  AppDelegate.h
//  MyAwesome_iOS_Applicattion
//
//  Created by Александр Малышев on 11.10.14.
//  Copyright (c) 2014 SecurityQQ. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

