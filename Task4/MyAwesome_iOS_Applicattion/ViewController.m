//
//  ViewController.m
//  MyAwesome_iOS_Applicattion
//
//  Created by Александр Малышев on 11.10.14.
//  Copyright (c) 2014 SecurityQQ. All rights reserved.
//
#import <Foundation/Foundation.h>
#import "ViewController.h"
#import "Password.h"

@interface ViewController ()

@end

@implementation ViewController

- (void) viewDidLoad {
    [super viewDidLoad];
    self.PasswordDifficultyProgressBar.progress = 0.0f;
    self.myTimer = [NSTimer scheduledTimerWithTimeInterval:1.0f
                                                    target:self
                                                  selector:@selector(changProgress)
                                                  userInfo:nil
                                                  repeats:YES];
}

- (void)changProgress {
    Password *pass = [[Password alloc]init];
    pass.password = [_PasswordTextField text];
    float difficulty = pass.getPasswordDifficulty;
    NSLog(@"%f", difficulty);
    self.PasswordDifficultyProgressBar.progress = difficulty;
    if (difficulty < 0.2) self.PasswordDifficultyProgressBar.progressTintColor = [UIColor redColor];
    if (difficulty >= 0.2 && difficulty < 0.6) self.PasswordDifficultyProgressBar.progressTintColor = [UIColor yellowColor];
    if (difficulty >= 0.6) self.PasswordDifficultyProgressBar.progressTintColor = [UIColor greenColor];
    
//    if (self.PasswordDifficultyProgressBar.progress == 1.0f) {
//        [myTimer invalidate];
//    }
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end
