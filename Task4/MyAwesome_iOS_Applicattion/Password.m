
//
//  Password.m
//  MyAwesome_iOS_Applicattion
//
//  Created by Александр Малышев on 25.10.14.
//  Copyright (c) 2014 SecurityQQ. All rights reserved.
//

#import "Password.h"

@implementation Password

- (float) getPasswordDifficulty {
    NSUInteger len = [_password length];
    NSUInteger sizeOfAlphabet = 0;
    NSCharacterSet *UCCharSet = [NSCharacterSet uppercaseLetterCharacterSet];
    NSCharacterSet *LCCharSet = [NSCharacterSet lowercaseLetterCharacterSet];
    NSCharacterSet *DCharSet = [NSCharacterSet decimalDigitCharacterSet];
    NSUInteger UCCharSetSize = 26, LCCharSetSize = 26, DCharSetSize = 10;
    NSRange tmp = [_password rangeOfCharacterFromSet: LCCharSet];
    if (tmp.length) sizeOfAlphabet += LCCharSetSize;
    tmp = [_password rangeOfCharacterFromSet: UCCharSet];
    if (tmp.length) sizeOfAlphabet += UCCharSetSize;
    tmp = [_password rangeOfCharacterFromSet: DCharSet];
    if (tmp.length) sizeOfAlphabet += DCharSetSize;
    float difficulty = len * log2(sizeOfAlphabet) / 100.0;
    if (difficulty > 0) return difficulty;
    return 0;
}

@end
