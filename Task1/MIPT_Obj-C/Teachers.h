//
//  Teachers.h
//  _Objective-C
//
//  Created by Александр Малышев on 24.09.14.
//  Copyright (c) 2014 SecurityQQ. All rights reserved.
//

#import "MIPT.h"

@interface Teacher : MIPT

@property BOOL isFriendly;
@property(nonatomic, readwrite) int averangeMark, freebie;
@property(nonatomic, readwrite) NSString *isFreebily, *name, *getAllInfo;
@end
