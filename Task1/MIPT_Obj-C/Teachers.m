//
//  Teachers.m
//  _Objective-C
//
//  Created by Александр Малышев on 24.09.14.
//  Copyright (c) 2014 SecurityQQ. All rights reserved.
//
#import <Cocoa/Cocoa.h>
#import "Teachers.h"

@implementation Teacher

+ (instancetype) instance {
    //TODO: understand the using of this method
    return [[self alloc] init];
}

- (instancetype)init{
    //setter
    if (!(self = [super init]))  return nil;
    _name = @"That guy";
    _freebie = random() % 10;
    return self;
}


- (NSString *)isFreebily{
    if (([_name isEqualTo:@"Beklemishev"]) || (_freebie < 5))
        return @"not";
    else return @"";
}

- (NSString *) getAllInfo{
    //getter
    assert(self.name);
    return [NSString stringWithFormat:@"%@ is %@ freebie", self.name, self.isFreebily];

}

@end
