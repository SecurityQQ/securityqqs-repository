//
//  WelcomeVC.m
//  SingleTimeHelloSegueTask
//
//  Created by Александр Малышев on 25.01.15.
//  Copyright (c) 2015 SecurityQQ. All rights reserved.
//

#import "WelcomeVC.h"

@interface WelcomeVC ()

@end

@implementation WelcomeVC

NSString* IsInitialWelcomeScreen = @"isInitialWelcomeScreen";

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUserDefaults:[NSUserDefaults standardUserDefaults]];
    [_showAgainWelcomeScreenSwitcher setOn: [_userDefaults boolForKey:IsInitialWelcomeScreen]];
}

//for debug
- (void) viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    NSLog(@"userDefaults: %d", [_userDefaults boolForKey:IsInitialWelcomeScreen]);
    if( ![_userDefaults boolForKey:IsInitialWelcomeScreen] ) {
        [self performSegueWithIdentifier:@"MainVCSegue" sender:self];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)showAgainWelcomeScreenSwitcher:(id)sender {
    [_userDefaults setBool:[_showAgainWelcomeScreenSwitcher isOn] forKey:IsInitialWelcomeScreen];
}


@end
