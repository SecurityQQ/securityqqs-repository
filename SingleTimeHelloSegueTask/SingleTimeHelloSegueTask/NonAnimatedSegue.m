//
//  NonAnimatedSegue.m
//  SingleTimeHelloSegueTask
//
//  Created by Александр Малышев on 27.01.15.
//  Copyright (c) 2015 SecurityQQ. All rights reserved.
//

#import "NonAnimatedSegue.h"

@implementation NonAnimatedSegue

- (void) perform {
    [[self sourceViewController] presentViewController:[self destinationViewController] animated:NO completion:nil];
}

@end
