//
//  AppDelegate.h
//  SingleTimeHelloSegueTask
//
//  Created by Александр Малышев on 25.01.15.
//  Copyright (c) 2015 SecurityQQ. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

