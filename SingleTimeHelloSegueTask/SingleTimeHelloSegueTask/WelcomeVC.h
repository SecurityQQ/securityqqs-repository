//
//  WelcomeVC.h
//  SingleTimeHelloSegueTask
//
//  Created by Александр Малышев on 25.01.15.
//  Copyright (c) 2015 SecurityQQ. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WelcomeVC : UIViewController
@property (weak, nonatomic) IBOutlet UIButton *changeSegueButton;
@property (weak, nonatomic) IBOutlet UISwitch *showAgainWelcomeScreenSwitcher;

@property NSUserDefaults* userDefaults;
@end
