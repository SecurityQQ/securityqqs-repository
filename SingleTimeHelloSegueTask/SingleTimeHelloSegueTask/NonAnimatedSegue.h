//
//  NonAnimatedSegue.h
//  SingleTimeHelloSegueTask
//
//  Created by Александр Малышев on 27.01.15.
//  Copyright (c) 2015 SecurityQQ. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NonAnimatedSegue : UIStoryboardSegue

@end
