//
//  UIImage+WebDownload.m
//  GCDTask
//
//  Created by Александр Малышев on 26.01.15.
//  Copyright (c) 2015 SecurityQQ. All rights reserved.
//

#import "UIImage+WebDownload.h"

@implementation UIImage (WebDownload)


- (instancetype) initWithContentsOfURL: (NSURL*) path {
    return [UIImage imageWithData:[NSData dataWithContentsOfURL:path]];
}

- (instancetype) initWithContentsOfURlWithString: (NSString*) path {
    NSURL* urlPath = [NSURL URLWithString: path];
    return [UIImage imageWithData:[NSData dataWithContentsOfURL:urlPath]];
}

@end
