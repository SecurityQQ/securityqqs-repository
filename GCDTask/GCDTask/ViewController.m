//
//  ViewController.m
//  GCDTask
//
//  Created by Александр Малышев on 26.01.15.
//  Copyright (c) 2015 SecurityQQ. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@interface UIImage (WebDownload)

- (instancetype) initWithContentsOfURL: (NSURL*) path;
- (instancetype) initWithContentsOfURlWithString: (NSString*) path;

@end

NSString* const kPath = @"http://93.175.16.58/web/objc/monkey_selfie.jpg";

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self downloadImageAsyncWithURL:[NSURL URLWithString:kPath] destinitionImageView:_imageFromWeb activityIndicator:_downloadingAIV];
    NSLog(@"I love monkeys!");
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];

}



- (void) downloadImageAsyncWithURL: (NSURL *) sourceURL destinitionImageView: (UIImageView *) imageView activityIndicator: (UIActivityIndicatorView *) activityIndicator {
    dispatch_queue_t queue = dispatch_queue_create("dlQueue", DISPATCH_QUEUE_SERIAL);
dispatch_async(queue, ^{
    
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
    [activityIndicator setHidesWhenStopped:YES];
    [activityIndicator startAnimating];
    
    NSURLRequest *request = [NSURLRequest requestWithURL:sourceURL];
    NSData * imageData = [[NSData alloc] initWithData:[NSURLConnection sendSynchronousRequest:request returningResponse:nil error:nil]];
    
    
    /// <#QUESTION#> What about async requests?
    //    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    //    [NSURLConnection sendAsynchronousRequest:request queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
    //        if( [data length] > 0 && connectionError == nil ) {
    //            [imageData appendData:data];
    //        } else
    //            if( [data length] == 0 && connectionError == nil) {
    //                NSLog(@"Nothing was downloaded");
    //            } else {
    //                if( connectionError != nil ) {
    //                    NSLog(@"Connection error");
    //                }
    //            }
    //    }];
    dispatch_async(dispatch_get_main_queue(), ^{
        [imageView setImage:[UIImage imageWithData: imageData]];
        [activityIndicator stopAnimating];
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
        NSLog(@"Image downloaded!");
    });

});
}

@end


