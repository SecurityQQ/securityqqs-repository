//
//  UIImage+WebDownload.h
//  GCDTask
//
//  Created by Александр Малышев on 26.01.15.
//  Copyright (c) 2015 SecurityQQ. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (WebDownload)

- (instancetype) initWithContentsOfURL: (NSURL*) path;
- (instancetype) initWithContentsOfURlWithString: (NSString*) path;

@end
