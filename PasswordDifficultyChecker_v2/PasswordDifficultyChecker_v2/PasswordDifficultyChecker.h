//
//  PasswordDifficultyChecker.h
//  PasswordDifficultyChecker_v2
//
//  Created by Александр Малышев on 09.03.15.
//  Copyright (c) 2015 SecurityQQ. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@protocol PasswordDifficultyCheckerProtocol <NSObject>
@required
- (void) passwordDidChecked: (float) passwordDifficulty;

@end

@interface PasswordDifficultyChecker : NSObject

@property (weak, nonatomic) id delegate;

- (void) checkPassword: (NSString*) aPassword;

@end
