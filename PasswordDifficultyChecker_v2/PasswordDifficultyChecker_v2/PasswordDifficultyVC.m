//
//  PasswordDifficultyVC.m
//  PasswordDifficultyChecker_v2
//
//  Created by Александр Малышев on 25.01.15.
//  Copyright (c) 2015 SecurityQQ. All rights reserved.
//

#import "PasswordDifficultyVC.h"

@interface PasswordDifficultyVC ()

@end

@implementation PasswordDifficultyVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [[self passwordTextField] setText:nil];
    _passwordChecker = [[PasswordDifficultyChecker alloc] init];
    [[self passwordChecker] setDelegate:self];
}
- (void) viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


- (IBAction) changeProcess:(id)sender {
    [[self passwordChecker] checkPassword:[[self passwordTextField] text]];
}


- (void) passwordDidChecked: (float) passwordDifficulty {
    [[self passwordDifficultyProgressBar] setProgress: passwordDifficulty];
    float progress = [_passwordDifficultyProgressBar progress];
    if( progress < 0.2 ) {
        [_passwordDifficultyProgressBar setTintColor: [UIColor redColor]];
    }
    if( progress >= 0.2 && progress < 0.6 ) {
        [_passwordDifficultyProgressBar setTintColor: [UIColor yellowColor]];
    }
    if( progress >= 0.6 ) {
        [_passwordDifficultyProgressBar setTintColor: [UIColor greenColor]];
    }
}


- (void) addTarget: (id) target action:(SEL)action forControlEvents:(UIControlEvents)controlEvents {
    
}

@end
