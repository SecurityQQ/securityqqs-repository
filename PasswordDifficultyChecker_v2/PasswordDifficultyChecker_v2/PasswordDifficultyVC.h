//
//  PasswordDifficultyVC.h
//  PasswordDifficultyChecker_v2
//
//  Created by Александр Малышев on 25.01.15.
//  Copyright (c) 2015 SecurityQQ. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PasswordDifficultyChecker.h"

@interface PasswordDifficultyVC : UIViewController <PasswordDifficultyCheckerProtocol>

@property (weak, nonatomic) IBOutlet UITextField *passwordTextField;
@property (weak, nonatomic) IBOutlet UIProgressView *passwordDifficultyProgressBar;
@property (strong, nonatomic) PasswordDifficultyChecker *passwordChecker;
@end
