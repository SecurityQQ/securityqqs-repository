//
//  PasswordDifficultyChecker.m
//  PasswordDifficultyChecker_v2
//
//  Created by Александр Малышев on 09.03.15.
//  Copyright (c) 2015 SecurityQQ. All rights reserved.
//

#import "PasswordDifficultyChecker.h"

@implementation PasswordDifficultyChecker

- (void) checkPassword: (NSString*) aPassword {
    float alphLen = 0;
    if ( [aPassword rangeOfCharacterFromSet:[NSCharacterSet alphanumericCharacterSet]].length) {
        if( [aPassword rangeOfCharacterFromSet:[NSCharacterSet uppercaseLetterCharacterSet]].length ) {
            alphLen += 26;
        }
        if( [aPassword rangeOfCharacterFromSet:[NSCharacterSet lowercaseLetterCharacterSet]].length ) {
            alphLen += 26;
        }
        if( [aPassword rangeOfCharacterFromSet:[NSCharacterSet decimalDigitCharacterSet]].length ) {
            alphLen += 10;
        }
        if( self.delegate ) {
            [self.delegate passwordDidChecked: ([aPassword length] * log2(alphLen) / 100.0)];
        }
    } else {
        if( self.delegate ) {
            [self.delegate passwordDidChecked: -1];
        }
    }
}


@end
