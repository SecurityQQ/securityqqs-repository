//
//  objectsGeneratorDelegate.m
//  AnimationsGame
//
//  Created by Александр Малышев on 31.01.15.
//  Copyright (c) 2015 SecurityQQ. All rights reserved.
//

#import "ObjectsGenerator.h"

const int imageSizeWidth = 200;
const int imageSizeHeight = 200;

@implementation ObjectsGenerator

- (void) generateView {
    [self generateProperties];
    [_generatedView setGestureRecognizers:_gestureRecognizers];
    [_generatedView setFrame:_frame];
    [_generatedView setBackgroundColor:_color];
    _index++;
    _maxIndex++;
    
    if( _delegate ) {
        [_delegate objectDidCreated: _generatedView index:_index];
    } else {
        NSLog(@"Can't find a delegate for view generating");
    }
}

- (void) generateProperties {
    _generatedView = [[UIView alloc] init];
    srand( (unsigned int)time(NULL) );
    NSNumber *recognizerSeed = [[NSNumber alloc] initWithInt: rand() % 4];
    [self generateGestureRecognizerWithSeed:recognizerSeed];
    [self generateColorWithFlag: (int)_generatedView.tag ];
    [self generateFrame];
}

- (UIColor *) generateColorWithFlag: (int) flag {
    switch( flag ) {
        case 0:
            [_generatedView setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"Left_Arrow.png"]]];
            break;
        case 1:
            [_generatedView setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"Right_Arrow.png"]]];
            break;
        case 2:
            [_generatedView setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"Up_Arrow.png"]]];
            break;
        case 3:
            [_generatedView setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"Down_Arrow.png"]]];
            break;
            
        default:
            [_generatedView setBackgroundColor:[UIColor blackColor]];
            break;
    }
    _color = _generatedView.backgroundColor;
    return _color;
}

- (NSArray *) generateGestureRecognizerWithSeed: (NSNumber *) recognizerSeed {
    if( _gestureRecognizers ) {
        _gestureRecognizers = nil;
    }
    _gestureRecognizers = [[NSMutableArray alloc] init];
    
    NSArray *directions = [NSArray arrayWithObjects:[NSNumber numberWithInt: UISwipeGestureRecognizerDirectionLeft],
                              [NSNumber numberWithInt: UISwipeGestureRecognizerDirectionRight],
                              [NSNumber numberWithInt: UISwipeGestureRecognizerDirectionUp],
                              [NSNumber numberWithInt: UISwipeGestureRecognizerDirectionDown],
                               nil];
    NSMutableArray *gestureRecognizers = [[NSMutableArray alloc] init];
    [_generatedView setTag: [recognizerSeed intValue]];
    
    for (int i = 0; i < directions.count; ++i ) {
        UISwipeGestureRecognizer *swipe;
        if( i == (int)_generatedView.tag ) {
            swipe = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(didCorrectSwipe:)];
        } else {
            swipe = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(didIncorrectSwipe:)];
        }
        swipe.direction = (UISwipeGestureRecognizerDirection) [directions[i] intValue];
        [gestureRecognizers addObject:swipe];
    }
    _gestureRecognizers = gestureRecognizers;
    return _gestureRecognizers;
    
}

- (void) didCorrectSwipe: (UISwipeGestureRecognizer *) swipe {
    assert(_delegate);
    [_delegate didCorrectSwipe:swipe];
}

- (void) didIncorrectSwipe: (UISwipeGestureRecognizer *) swipe {
    assert(_delegate);
    [_delegate didIncorrectSwipe:swipe];
}

- (CGRect) generateFrame {
// <#Question:#> I tried to do with:
//    CGRect screenRect = [[UIScreen mainScreen] applicationFrame];
// but it didn't work well, so what can I do for better result?
    _frame = CGRectMake(       rand() % 150,
                               rand() % 250,
                               imageSizeWidth,
                               imageSizeHeight
                        );
    
    return _frame;
}




@end
