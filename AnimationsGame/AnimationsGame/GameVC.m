//
//  GameVC.m
//  AnimationsGame
//
//  Created by Александр Малышев on 30.01.15.
//  Copyright (c) 2015 SecurityQQ. All rights reserved.
//

#import "GameVC.h"
#import <AudioToolbox/AudioToolbox.h>

#pragma mark - private interface
@interface GameVC ()

@property (weak) IBOutlet UIView *mainView;
@property (strong) ObjectsGenerator *generator;
@property (weak, nonatomic) IBOutlet UITextView *scoreTextView;
@property (assign, nonatomic) int playerScore;
@property (weak, nonatomic) NSTimer *gameTimer;
@property (strong, nonatomic)    NSNotificationCenter *notificationCenter;

@end


#pragma mark - constants
NSString const *GenerateNotification = @"GenerateNotification";
NSString const *ExitSegueIdentifier = @"exit";
const int MaxViews = 5;
const int WinningScore = 1000;
const int LoosingScore = -WinningScore;
float spawningInterval;


#pragma mark - implementation
@implementation GameVC

 
- (void) viewDidLoad {
    [super viewDidLoad];
    _playerScore = 0;
    spawningInterval = 2.0f;
    _generator = [[ObjectsGenerator alloc] init];
    [_generator setDelegate:self];
}


- (void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
}


- (void) viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self startTheGame];
}


- (void) startTheGame {
    _gameTimer = [NSTimer scheduledTimerWithTimeInterval:spawningInterval target:self selector:@selector(generateAsync) userInfo:nil repeats:YES];
}


- (void) endGame {
    [_gameTimer invalidate];
    [self deallocSubviews];
}


- (IBAction)exitButtonPressed:(id)sender {
    [self endGame];
}


- (void) generateAsync {
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        [_generator generateView];
        [self changeSpawningInterval: &spawningInterval];
    });
}


- (void) changeSpawningInterval: (float *) spawningInterval {
    *spawningInterval = fmax(0.7f, *spawningInterval - 0.3f);
}


- (void) deallocSubviews {
    for( int i = 1; i < _mainView.subviews.count; ++i ) {
        [_mainView.subviews[i] removeFromSuperview];
    }

}



- (void) objectDidCreated:(UIView *)view index:(int)index {
    dispatch_async(dispatch_get_main_queue(), ^{
        view.tag = index;
        [[self mainView] addSubview:view];
        
        [UIView animateWithDuration:0.3 delay:0 options:UIViewAnimationOptionCurveLinear animations:^{
            view.transform = CGAffineTransformMakeScale(1.1f, 1.1f);
        } completion:^(BOOL finished){
             [UIView animateWithDuration:0.3 delay:0 options:UIViewAnimationOptionCurveLinear animations:^{
                view.transform = CGAffineTransformMakeScale(1 / 1.1f, 1 / 1.1f);
                    } completion:^(BOOL finished){
             }];
        }];
    });
    if( [_mainView subviews].count > MaxViews) {
        [self exitTheGameWithAlertUsingTitle:@"YOU LOST" AndMessage:@"You lost, sorry"];
    }
}


- (void) didCorrectSwipe: (UISwipeGestureRecognizer *) swipe {
    [self deleteLastViewSwipedBy: swipe];
    [self addScore: 50 * (spawningInterval + 0.3) ];
}


- (void) didIncorrectSwipe: (UISwipeGestureRecognizer *) swipe {
    AudioServicesPlaySystemSound(kSystemSoundID_Vibrate);
    [self deleteLastViewSwipedBy: swipe];
    [self addScore: -50 * (1 / spawningInterval)];
}


- (void) deleteLastViewSwipedBy: (UISwipeGestureRecognizer *) swipe {
    UIView *subview = [self.mainView viewWithTag:swipe.view.tag];
    [UIView animateWithDuration:0.3 delay:0 options:UIViewAnimationOptionCurveLinear animations:^{
        subview.transform = CGAffineTransformMakeScale(1.1f, 1.1f);
    } completion:^(BOOL finished){
        [UIView animateWithDuration:0.3 delay:0 options:UIViewAnimationOptionCurveLinear animations:^{
            subview.transform = CGAffineTransformMakeScale(1 / 1.1f, 1 / 1.1f);
        } completion:^(BOOL finished){
            [subview removeFromSuperview];
        }];
    }];
}


- (void) addScore:(int) score{
    _playerScore += score;

    if( _playerScore >= WinningScore ) {
        [self exitTheGameWithAlertUsingTitle:@"Сongratulations!" AndMessage:@"You won."];
    } else {
        if (_playerScore < LoosingScore ) {
            [self exitTheGameWithAlertUsingTitle:@"YOU LOST" AndMessage:@"Sorry"];
        } else {
            if( score > 0 ) {
                    [_scoreTextView setTextColor:[UIColor greenColor]];
                } else {
                    [_scoreTextView setTextColor:[UIColor redColor]];
                }
                [_scoreTextView performSelector:@selector(setTextColor:) withObject:nil afterDelay:0.5f];
                [_scoreTextView setText:[NSString stringWithFormat:@"%d", _playerScore]];
        }
    }
}


- (void) exitTheGameWithAlertUsingTitle: (NSString *) title AndMessage: (NSString *) message {
    dispatch_async(dispatch_get_main_queue(), ^{
        [self performSegueWithIdentifier:@"exit" sender:self];
        UIAlertView *gameOverAlert = [[UIAlertView alloc]
                                      initWithTitle:title
                                      message:message delegate:self cancelButtonTitle:@"Exit" otherButtonTitles: nil];
        [_gameTimer invalidate];
        [gameOverAlert show];
    });
    
    //Deleting all subviews:
    [self deallocSubviews];
}


@end
