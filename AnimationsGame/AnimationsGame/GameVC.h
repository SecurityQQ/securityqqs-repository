//
//  GameVC.h
//  AnimationsGame
//
//  Created by Александр Малышев on 30.01.15.
//  Copyright (c) 2015 SecurityQQ. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ObjectsGenerator.h"

@interface GameVC : UIViewController <ObjectsGeneratorDelegate>

@end
