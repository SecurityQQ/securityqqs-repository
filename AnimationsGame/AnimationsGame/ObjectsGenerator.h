//
//  objectsGeneratorDelegate.h
//  AnimationsGame
//
//  Created by Александр Малышев on 31.01.15.
//  Copyright (c) 2015 SecurityQQ. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@protocol ObjectsGeneratorDelegate

@required
- (void) objectDidCreated: (UIView *) view index: (int) index;
- (void) didCorrectSwipe: (UISwipeGestureRecognizer *) swipe;
- (void) didIncorrectSwipe: (UISwipeGestureRecognizer *) swipe;

@end

@interface ObjectsGenerator : NSObject

@property (weak, nonatomic) id delegate;
@property (nonatomic) int index;
@property (nonatomic) float seed;
@property (readonly, nonatomic) int maxIndex;
@property (weak, nonatomic) UIView *parentView;

//Generating properties:
@property (strong, nonatomic) UIView *generatedView;
@property (strong, nonatomic) UIColor *color;
@property (strong, nonatomic) NSMutableArray *gestureRecognizers;
@property (      nonatomic) CGRect frame;

#pragma mark - generationMethods:
- (void) generateView;
@end

