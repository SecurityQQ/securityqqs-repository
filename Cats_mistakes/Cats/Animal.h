//
//  Animal.h
//  FirstLesson
//
//  Created by artemiy on 20.09.14.
//  Copyright (c) 2014 mipt. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Animal : NSObject <NSCoding>

@property (nonatomic) int hungerLevel;
@property(nonatomic) BOOL isMale;

@end
