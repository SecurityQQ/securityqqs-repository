//
//  Cat.m
//  FirstLesson
//
//  Created by artemiy on 13.09.14.
//  Copyright (c) 2014 mipt. All rights reserved.
//

#import "Cat.h"
NSString * const CatNickNameKey = @"cat nick name";
NSString * const CatNumberOfColorsKey = @"cat number of colors";


@implementation Cat

#pragma mark - Initialisation
// 7th:
- (instancetype)init{
        if (!(self = [super init]))  return nil;
        _nickName = [Cat defaultCatName];
        _numberOfColors = 1;
    return self;
    }
- (instancetype)initWithNumberOfColors:(int)numberOfColors{
    if (numberOfColors > 3 || numberOfColors < 1) return nil;
    if (!(self = [self init]))  return nil;
    _numberOfColors = numberOfColors;
    //5th Mistake here:
    _nickName = [Cat defaultCatName];
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder{
    if (!(self = [super initWithCoder:aDecoder])) return nil;
    _nickName = [aDecoder decodeObjectForKey:CatNickNameKey];
    _numberOfColors = [aDecoder decodeIntForKey:CatNumberOfColorsKey];
    return self;
}

#pragma mark - saving
//3rd mistake:
- (void)encodeWithCoder:(NSCoder *)aCoder{
    [super encodeWithCoder:aCoder];
    [aCoder encodeObject:self.nickName forKey:CatNickNameKey];
    // (Wrong key)
    //[aCoder encodeObject:self.nickName forKey:CatNumberOfColorsKey];
    [aCoder encodeInteger:self.numberOfColors forKey:CatNumberOfColorsKey];
}

#pragma mark - model

//4th mistake:
- (void)setIsMale:(BOOL)isMale {
    if (isMale)
        return;
    [super setIsMale:isMale];
}

- (void)setNumberOfColors:(int)numberOfColors{
    if (numberOfColors > 3 || numberOfColors < 1) {
        return;
    }
    else if (numberOfColors == 3 && self.isMale){
        return;
    }
    _numberOfColors = numberOfColors;
}

- (BOOL)isEqualToCat:(Cat *)cat{
    if (self == cat) return YES;
    if (self.isMale != cat.isMale) return NO;
    //6th mistake:
    if (![self.nickName isEqualToString: cat.nickName]) return NO;
    //if (self.nickName != cat.nickName) return NO;
    if (self.numberOfColors != cat.numberOfColors) return NO;
    
    return YES;
}

#pragma mark - other methods

+ (NSString *)defaultCatName{
    return @"murmur";
}

- (NSString *)description{
    assert(self.nickName);
    return [NSString stringWithFormat:@"cat name:%@ %d %d", self.nickName, self.isMale, self.numberOfColors];
}

@end
