//
//  main.m
//  Cats
//
//  Created by Artemiy on 29.09.14.
//  Copyright (c) 2014 Artemiy. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, const char * argv[]) {
    return NSApplicationMain(argc, argv);
}
